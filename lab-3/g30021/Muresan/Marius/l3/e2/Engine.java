
package g30021.Muresan.Marius.l3.e2;

public class Engine {

    String fuellType;
    long capacity;
    boolean active;
    int power;

    Engine(int capacity,boolean active,int p){
        this.capacity = capacity;
        this.active = active;
        this.power=p;
    }
    Engine(int capacity,boolean active,int p, String fuellType){
        this(capacity,active,p);
        this.fuellType = fuellType;
    }
    Engine(){
        this(2000,false,100,"Diesel");
    }
    void print(){
        System.out.println("Engine: capacity="+this.capacity+" fuell="+fuellType+" active="+active+ " p="+power);
    }
    public static void main(String[] args) {
        Engine tdi = new Engine();
        Engine i16 = new Engine(1600,false,75,"petrol");
        Engine d30 = new Engine(3000,true,120,"diesel");
        tdi.print();i16.print();d30.print();
    }
}
