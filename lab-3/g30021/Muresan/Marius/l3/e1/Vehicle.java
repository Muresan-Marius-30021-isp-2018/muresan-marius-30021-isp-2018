
package g30021.Muresan.Marius.l3.e1;

public class Vehicle {
    String marca;
    String model;
    int viteza=0;
    String culoare;
    void accelerare (int val){
        viteza = viteza + val;

    }
    void franare(int val){
        viteza=viteza-val;
    }
    void stare()
    {
        System.out.println("\nMarca: "+marca+"\nModel: "+model+"\nViteza: "+viteza+"\nCuloare: "+culoare);
    }
    public static void main(String[] args) {

        Vehicle masina1= new Vehicle();
        Vehicle masina2= new Vehicle();

        masina1.marca="Volkswagen";
        masina1.model="Golf";
        masina1.culoare="Galben";
        masina1.accelerare(80);
        masina1.franare(40);
        masina1.stare();


        masina2.marca="Ford";
        masina2.model="Mondeo";
        masina2.culoare="Rosu";
        masina2.accelerare(90);
        masina2.franare(10);
        masina2.stare();
    }

}
