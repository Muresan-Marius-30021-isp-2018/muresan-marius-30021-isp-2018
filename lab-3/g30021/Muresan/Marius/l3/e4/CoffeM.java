
package g30021.Muresan.Marius.l3.e4;


public class CoffeM {
     public static void main(String[] args)
    {
        CoffeMaker maker1 = new CoffeMaker();

        Coffee[] pachet = new Coffee[10];
        for (int i = 0; i < pachet.length; i++)
        {
            pachet[i] = maker1.getCofee();
        }

        for (int i = 0; i < pachet.length; i++)
        {
            pachet[i].drinkCofee();
        }

    }
}

class CoffeMaker
{

    CaffeineTank ctank = new CaffeineTank();
    WaterTank wtank = new WaterTank();
    WaterBoiling WT = new WaterBoiling();

    CoffeMaker()
    {
        System.out.println("New cofee maker created.");
    }

    Coffee getCofee()
    {
        int w = wtank.getIngredient();
        int c = ctank.getIngredient();
        int temp = WT.boiling(WT.randomTempGen());

        return new Coffee(w, c, temp);
    }
}

class CaffeineTank
{

    CaffeineTank()
    {
        System.out.println("New coffeine tank created.");
    }

    int getIngredient()
    {
        return (int) (Math.random() * 10);
    }
}

class WaterTank
{

    WaterTank()
    {
        System.out.println("New water tank created.");
    }

    int getIngredient()
    {
        return (int) (Math.random() * 40);
    }
}

class WaterBoiling
{

    WaterBoiling()
    {
        System.out.println("The boiling system was created.");
    }

    int boiling(int tempR)
    {
        return tempR;
    }

    int randomTempGen()
    {
        return (int) (50 + Math.random() * 30);
    }

}

class Coffee
{

    int water;
    int caffeine;
    int temp;

    Coffee(int water, int caffeine, int tempBoiling)
    {
        this.water = water;
        this.caffeine = caffeine;
        this.temp = tempBoiling;
    }

    void drinkCofee()
    {
        System.out.println("Drink cofee (water=" + water + ":coffe=" + caffeine + ":temp=" + temp + ")");
    }
}
