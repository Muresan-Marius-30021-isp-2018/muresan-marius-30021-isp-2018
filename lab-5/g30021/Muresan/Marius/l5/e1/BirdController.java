
package g30021.Muresan.Marius.l5.e1;

class Bird {
            public void move(){
                        System.out.println("The bird is moving.");
            }
}
class Sparrow extends Bird
{
    public void move(){
                    System.out.println("The SPARROW is flying");
    }
}
class Penguin extends Bird{
            public void move(){
                        System.out.println("The PENGUIN is swiming.");
            }
}

class Goose extends Bird{
            public void move(){
                        System.out.println("The GOOSE is flying.");
            }
}

public class BirdController {



    Bird[] birds = new Bird[5];
            BirdController(){
                        birds[0] = createBird();
                        birds[1] = createBird();
                        birds[2] = createBird();
                        birds[3] = createBird();
                        birds[4] = createBird();
            }
            public void relocateBirds(){
                        for(int i=0;i<birds.length;i++)
                                    birds[i].move();
            }

            private Bird createBird(){
                        int i = (int)(Math.random()*10);
                        if(i<5)
                                    return new Penguin();
                        else if(i==5)
                                    return new Goose();
                        else
                            return new Sparrow();



            }

            public static void main(String [] args){
                        BirdController bc = new BirdController();
                        bc.relocateBirds();
            }
    }


