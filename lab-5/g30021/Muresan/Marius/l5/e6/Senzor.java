
package g30021.Muresan.Marius.l5.e6;


public class Senzor {
    int value;
    String location="rooftop";
    
    
    int getValue(){
        return value;
    }
    void setValue(int v){
        this.value=v;
    }
    void DisplaySenzorInfo(){
        System.out.println("Senzor value is: "+value+" \nlocation: "+location);
    }

 public static void main(String [] args){

     Senzor n=new Senzor();
     n.getValue();
     n.setValue(44);
     
     n.DisplaySenzorInfo();
 }
}