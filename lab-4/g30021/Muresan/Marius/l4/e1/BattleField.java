
package g30021.Muresan.Marius.l4.e1;

public class BattleField
{

    Robot r1;
    Robot r2;

    BattleField()
    {
        r1 = new Robot();
        r2 = new Robot();
    }

    public void play()
    {
        r1.moveRobot(15, 3);
        r2.moveRobot(-10, -5);
    }

    public static void main(String[] args)
    {
        BattleField game = new BattleField();
        game.play();
    }
}

