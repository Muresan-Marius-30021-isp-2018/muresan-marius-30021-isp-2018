
package g30021.Muresan.Marius.l4.e3;


public class Professor extends Person {
    
    int nrprofessor;
    public Professor(int nrp,String nume,int age)
    {
        super(nume,age);
        nrprofessor=nrp;
    }
    public int nrprofessor()
    {
        return nrprofessor;
    }
    public String af()
    {
        return "Nr. Professor:"+nrprofessor()+"\nProfessor name:"+this.gtname()+"\nAge"+this.gtage();
    }
}
