
package g30021.Muresan.Marius.l4.e4;

/**
 *
 * @author Muresan
 */
public class LinkedList {
    
public class Node {

    int data;
    Node next;

}
    Node head;

    public void ins(int data) {
        Node node = new Node();
        node.data = data;
        node.next = null;
        if (head == null) {
            head = node;
        } else {
            Node n = head;
            while (n.next != null) {
                n = n.next;
            }
            n.next = node;
        }
    }

    public void out() {
        Node node = head;
        while (node.next != null) {
            System.out.println(node.data);
            node = node.next;
        }
        System.out.println(node.data);

    }
    
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.ins(55);
        list.ins(345);
        list.ins(5);
        list.ins(4);
        list.out();
    }

}
